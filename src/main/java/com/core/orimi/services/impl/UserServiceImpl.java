package com.core.orimi.services.impl;

import java.util.List;

import com.core.orimi.models.User;
import com.core.orimi.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.core.orimi.services.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
    UserRepo userRepo;
	
	public List<User> findallUsers() {
		// TODO Auto-generated method stub
		return userRepo.findAll();
	}

	public User findByUserName(String userName) {
		// TODO Auto-generated method stub
		return userRepo.findByUserName(userName);
	}

	public User save (User user) {
		// TODO Auto-generated method stub
		return userRepo.save(user);
	}

}
