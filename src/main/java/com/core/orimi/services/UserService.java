package com.core.orimi.services;

import java.util.List;

import com.core.orimi.models.User;

public interface UserService {
	List<User> findallUsers();
	User findByUserName(String userName);
	User save(User user);
	
}
