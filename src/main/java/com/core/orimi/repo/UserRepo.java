package com.core.orimi.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.core.orimi.models.User;

@Repository
public interface UserRepo  extends CrudRepository<User, Integer>{
	List<User> findAll();
	
	User findByUserName(String userName);
	
	User findByUserId(Integer userId);
	
	User save(User user);

}
