package com.core.orimi.controllers;

import java.util.List;

import com.core.orimi.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.core.orimi.services.UserService;

@RestController
@RequestMapping ("/rest")
public class UserResource {
	
	@Autowired 
	private UserService userService;
	
	@RequestMapping ("/user/users")
	public List<User> findallUsers() {
		return userService.findallUsers();
	}

}
